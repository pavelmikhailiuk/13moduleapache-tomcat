install apache tomcat8 server, installation path should not contain spaces. confugured default path is D:\apache-tomcat-8.0.32
change tomcat confuguration files:

TomcatDir/conf/tomcat-users.xml --> add or uncomment and change
<role rolename="manager-gui"/>
<role rolename="manager-script"/>

<user roles="manager-gui,manager-script" password="tomcat" username="tomcat"/>
TomcatDir/conf/server.xml --> uncomment
<Connector port="8009" redirectPort="8443" protocol="AJP/1.3"/> if commented

start tomcat server with java commandline args -Dspring.profiles.active=JPA

install apache http server, version 2.4 is preferable, installation path should not contain spaces. confugured default path is C:\Apache24
create a file ApacheDir/conf/extra/workers.properties, add properties below to it

worker.list=tomcat
worker.ajp13.port=8009
worker.ajp13.host=localhost
worker.ajp13.type=ajp13 

modify a file ApacheDir/conf/httpd.conf -->

check -->
ServerRoot "c:/Apache24" is correct and points to your apache installation
change -->
Listen 80 to Listen 127.0.0.1:80
add or uncomment -->
LoadModule jk_module modules/mod_jk.so
if you added above string, check ApacheDir/modules/mod_jk.so exists. Download it and put there if not.

add -->
<IfModule jk_module>
JkWorkersFile conf/extra/workers.properties
JkLogFile logs/mod_jk.log
JkLogLevel info
JkLogStampFormat "[%a %b %d %H:%M:%S %Y]"
JkMount /newsadmin/* tomcat
JkMount /newsadmin tomcat
JkUnMount /newsadmin/resources/* tomcat
JkMount /newsclient/* tomcat
JkMount /newsclient tomcat
JkUnMount /newsclient/js/* tomcat
JkUnMount /newsclient/css/* tomcat
</IfModule>

change -->
<Directory "c:/Apache24/htdocs"> section

remove Indexes from uncommented part

change your maven settings.xml file --> 
uncomment below in <servers> section
and change
<server>
  <id>TomcatServer</id>
 
  <username>tomcat</username>
 
  <password>tomcat</password>
 
</server>

download the project
check newsmanagement/pom.xml contains valid path to your apache installation and above server section provided for maven
match server, username and password provided for maven-tomcat plugin

run deploy.bat




