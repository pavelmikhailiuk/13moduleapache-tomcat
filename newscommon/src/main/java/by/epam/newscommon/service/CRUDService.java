package by.epam.newscommon.service;

import java.util.List;

import by.epam.newscommon.exception.ServiceException;

// TODO: Auto-generated Javadoc
/**
 * The Interface CRUDService.
 *
 * @param <T> the generic type
 */
public interface CRUDService<T> {
	
	/**
	 * Save.
	 *
	 * @param entity the entity
	 * @return the long id
	 * @throws ServiceException the service exception if operation failed
	 */
	Long save(T entity) throws ServiceException;

	/**
	 * Find.
	 *
	 * @param id the id
	 * @return the the generic type
	 * @throws ServiceException the service exception if operation failed
	 */
	T find(Long id)throws ServiceException;

	/**
	 * Update.
	 *
	 * @param entity the entity
	 * @return the the generic type
	 * @throws ServiceException the service exception if operation failed
	 */
	T update(T entity)throws ServiceException;

	/**
	 * Find all.
	 *
	 * @return the list of the generic type
	 * @throws ServiceException the service exception if operation failed
	 */
	List<T> findAll()throws ServiceException;
	
	/**
	 * Delete.
	 *
	 * @param id the id
	 * @throws ServiceException the service exception if operation failed
	 */
	void delete(Long id)throws ServiceException;
}
