package by.epam.newscommon.service;

import by.epam.newscommon.entity.Author;

/**
 * The Interface AuthorService. Extends interface CRUDService
 */
public interface AuthorService extends CRUDService<Author> {

}
