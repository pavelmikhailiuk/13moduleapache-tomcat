package by.epam.newscommon.service;

import by.epam.newscommon.entity.Tag;

/**
 * The Interface TagService. Extends interface CRUDService
 */
public interface TagService extends CRUDService<Tag> {

}
