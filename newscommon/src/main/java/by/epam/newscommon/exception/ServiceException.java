package by.epam.newscommon.exception;

/**
 * The Class ServiceException.
 */
public class ServiceException extends Exception {
	/** The Constant serialVersionUID. */
	public static final long serialVersionUID = 1L;

	
	/**
	 * Instantiates a new service exception.
	 *
	 * @param message the message
	 * @param e the Throwable object e
	 */
	public ServiceException(String message, Throwable e) {
		super(message, e);
	}
}
