package by.epam.newscommon.dao;

import by.epam.newscommon.entity.Author;

/**
 * The Interface AuthorDAO. Extends interface CRUDDAO
 */
public interface AuthorDAO extends CRUDDAO<Author> {

}
