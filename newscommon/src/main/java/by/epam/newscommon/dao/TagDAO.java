package by.epam.newscommon.dao;

import by.epam.newscommon.entity.Tag;

/**
 * The Interface TagDAO. Extends interface CRUDDAO
 */
public interface TagDAO extends CRUDDAO<Tag> {

}
