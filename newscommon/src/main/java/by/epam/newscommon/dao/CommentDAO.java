package by.epam.newscommon.dao;

import by.epam.newscommon.entity.Comment;
import by.epam.newscommon.exception.DAOException;


/**
 * The Interface CommentDAO. Extends interface CRUDDAO
 */
public interface CommentDAO extends CRUDDAO<Comment> {
	
	/**
	 * Delete comments by news id.
	 *
	 * @param id the array of the news id
	 * @throws DAOException the DAO exception if operation failed.
	 */
	void deleteByNewsId(Long[] id) throws DAOException;
}
