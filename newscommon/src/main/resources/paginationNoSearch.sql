select * from ( select a.*, ROWNUM rnum from (SELECT news.news_id, news.title, news.short_text, news.full_text,
news.creation_date, news.modification_date, COUNT(comments.news_id) AS commentCount FROM news LEFT JOIN
comments ON news.news_id=comments.news_id GROUP BY news.news_id, news.title, news.short_text, news.full_text,
news.creation_date, news.modification_date ORDER BY commentCount DESC, news.modification_date DESC)
a where ROWNUM <=4 ) where rnum  > 3;