<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>


<tiles:insertDefinition name="defaultTemplate">
	<tiles:putAttribute name="body">
		<div class="body">
			<h1>HTTP Status 403 - <spring:message code="access.denied" /></h1>

	<c:choose>
		<c:when test="${empty username}">
		  <h2><spring:message code="access.permission" /></h2>
		</c:when>
		<c:otherwise>
		  <h2><spring:message code="access.username" /> : ${username} <br/>
                    <spring:message code="access.permission" /></h2>
		</c:otherwise>
	</c:choose>
			
		</div>
	</tiles:putAttribute>
</tiles:insertDefinition>