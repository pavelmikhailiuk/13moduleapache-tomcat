<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<tiles:insertDefinition name="defaultTemplate">
	<tiles:putAttribute name="body">
		<div class="body">
			<c:url value="/admin/processtag" var="processTag" />
			<form:form name="edit" action="${processTag}" method="post"
				modelAttribute="tag">
				<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}" />
				<form:hidden path="tagId" />
				<form:errors path="tagName" class="error" />
				<c:forEach var="tag" items="${tags}">
					<table class="add-author">
						<tbody>
							<tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td><form:label path="tagName">
										<b><spring:message code="tag.tag" /></b>
									</form:label></td>
								<td><c:choose>
										<c:when test="${editTagId eq tag.tagId}">
											<form:textarea name="tagName" path="tagName" rows="1"
												cols="40" maxlength="30"></form:textarea>
										</c:when>
										<c:otherwise>
											<textarea disabled="disabled" rows="1" cols="40"><c:out
													value="${tag.tagName}" /></textarea>
										</c:otherwise>
									</c:choose></td>
								<td><c:choose>
										<c:when test="${editTagId eq tag.tagId}">
											<a href="javascript:document.edit.submit();"><spring:message
													code="update.tag" /></a>
											<a href="<c:url value="/admin/addtag?tagId=${tag.tagId}"/>"><spring:message
													code="delete.tag" /></a>
											<a href="<c:url value="/admin/addtag"/>"><spring:message
													code="cancel.tag" /></a>
										</c:when>
										<c:otherwise>
											<a
												href="<c:url value="/admin/edittag/${tag.tagId}?tagName=${tag.tagName}"/>"><spring:message
													code="edit.tag" /></a>
										</c:otherwise>
									</c:choose></td>
							</tr>
						</tbody>
					</table>
				</c:forEach>

				<table class="add-author">
					<tbody>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td><form:label path="tagName">
									<b><spring:message code="add.tag" /></b>
								</form:label></td>
							<td><c:choose>
									<c:when test="${editTagId == null}">
										<form:textarea name="tagName" path="tagName" rows="1"
											cols="40"></form:textarea>
									</c:when>
									<c:otherwise>
										<textarea disabled="disabled" rows="1" cols="40"></textarea>
									</c:otherwise>
								</c:choose></td>
							<td><a
								href="javascript: if(validateTagForm()){document.edit.submit();}"><spring:message
										code="save.tag" /></a></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
					</tbody>
				</table>

			</form:form>
		</div>
	</tiles:putAttribute>
</tiles:insertDefinition>
