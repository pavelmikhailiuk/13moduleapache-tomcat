<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<tiles:insertDefinition name="defaultTemplate">
	<tiles:putAttribute name="body">
		<div class="body">
			<div class=back-link>
				<a href="<c:url value="/admin/listnews?pageNumber=${currentPage}"/>"><spring:message
						code="back" /></a>
			</div>

			<c:set var="news" value="${newsDTO.news}" />
			<c:set var="prevNews" value="${prevNewsDTO.news}" />
			<c:set var="nextNews" value="${nextNewsDTO.news}" />
			<c:set var="author" value="${newsDTO.author}" />
			<c:set var="commentList" value="${newsDTO.comments}" />

			<table class="list-news">
				<tr>

					<td><b><c:out value="${news.title}" /></b> ( <fmt:message
							key="author" /> <c:out value="${author.authorName}" /> )</td>
					<td class="news-list-date"><fmt:formatDate
							value="${news.modificationDate}" /></td>
				</tr>
				<tr>

					<td class="news-list-brief" colspan="2"><c:out
							value="${news.fullText}" /></td>
				</tr>
			</table>

			<c:forEach var="comment" items="${commentList}">
				<form action="<c:url value="/admin/deletecomment"/>" method="post">
					<input type="hidden" name="${_csrf.parameterName}"
						value="${_csrf.token}" />
					<fmt:formatDate value="${comment.creationDate}" />
					<div class="comment-text">
						<c:out value="${comment.commentText}" />
						<button class="del-comment-button" type="submit" name=commentId
							value="${comment.commentId}">
							<spring:message code="delete.symbol" />
						</button>
						<input type="hidden" name=newsId value="${news.newsId}">
					</div>
				</form>
			</c:forEach>
			<form name="commentForm" action="<c:url value="/admin/addcomment"/>"
				method="post" onsubmit="return validateCommentForm();">
				<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}" />
				<textarea class="textarea" cols="40" rows="5" name="commentText"
					maxlength="100"></textarea>
				<input type="hidden" name="newsId" value="${news.newsId}" /> <br />
				<c:if test="${error !=null}">
					<span class="error"> <fmt:message key="error.comment" /></span>
				</c:if>
				<button class="post-comment-button" type="submit">
					<spring:message code="post.comment" />
				</button>
			</form>

			<c:if test="${prevNews != null}">
				<div class="prev">
					<a
						href="<c:url value="/admin/shownews/${prevNews.newsId}/?direction=prev"/>"><spring:message
							code="previous" /></a>
				</div>
			</c:if>

			<c:if test="${nextNews != null}">
				<div class="next">
					<a
						href="<c:url value="/admin/shownews/${nextNews.newsId}/?direction=next"/>"><spring:message
							code="next" /></a>
				</div>
			</c:if>

		</div>
	</tiles:putAttribute>
</tiles:insertDefinition>
