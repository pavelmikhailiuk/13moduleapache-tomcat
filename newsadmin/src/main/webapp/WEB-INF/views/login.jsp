<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>

<tiles:insertDefinition name="login">
	<tiles:putAttribute name="body">
		<div class="login-body">
			<div class="login">
				<c:if test="${not empty error}">
					<div class="error">
						<spring:message code="login.error" />
					</div>
				</c:if>
				<c:if test="${not empty logout}">
					<div class="msg">
						<spring:message code="login.success" />
					</div>
				</c:if>
				<form name='loginForm' action='j_spring_security_check'
					method='POST'>
					<table>
						<tr>
							<td><spring:message code="login.login" /></td>
							<td align="right"><input type='text' size="19" name='login'></td>
						</tr>
						<tr>
							<td><spring:message code="login.password" /></td>
							<td align="right"><input type='password' size="20"
								name='password' /></td>
						</tr>
						<tr>
							<td colspan="2" align="right"><input name="submit"
								type="submit" value="<spring:message code="button.login" />" /></td>
						</tr>
					</table>
					<input type="hidden" name="${_csrf.parameterName}"
						value="${_csrf.token}" />
				</form>
			</div>

		</div>
	</tiles:putAttribute>
</tiles:insertDefinition>
