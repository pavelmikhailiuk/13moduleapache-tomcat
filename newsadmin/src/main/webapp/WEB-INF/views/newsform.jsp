<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<tiles:insertDefinition name="defaultTemplate">
	<tiles:putAttribute name="body">
		<div class="body">
			<c:url value="/admin/processnews" var="processnews" />
			<form:form name="newsForm" action="${processnews}" method="post"
				modelAttribute="newsDTO" onsubmit="return validateNewsForm();">
				<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}" />
				<form:hidden path="news.newsId" />
				<form:hidden path="news.creationDate" />
				<table class="add-news">
					<tbody>
						<tr>
							<td>&nbsp;</td>
							<td><form:errors path="news.title" class="error" /></td>
						</tr>
						<tr>
							<td><form:label path="news.title">
									<b><spring:message code="addnews.title" /></b>
								</form:label></td>
							<td><form:textarea path="news.title" cols="60" rows="1"
									maxlength="30"></form:textarea></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td><form:label path="news.modificationDate">
									<b><spring:message code="addnews.date" /></b>
								</form:label></td>
							<td><form:input path="news.modificationDate"></form:input></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td><form:errors path="news.shortText" class="error" /></td>
						</tr>
						<tr>
							<td><form:label path="news.shortText">
									<b><spring:message code="addnews.brief" /></b>
								</form:label></td>
							<td><form:textarea path="news.shortText" rows="5" cols="60"
									maxlength="100"></form:textarea></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td><form:errors path="news.fullText" class="error" /></td>
						</tr>
						<tr>
							<td><form:label path="news.fullText">
									<b><spring:message code="addnews.content" /></b>
								</form:label></td>
							<td><form:textarea path="news.fullText" rows="10" cols="60"
									maxlength="2000"></form:textarea></td>
						</tr>
					</tbody>
				</table>

				<div class="error">
					<c:if test="${selectAuthorMsg!=null}">
						<spring:message code="select.author.message" />
					</c:if>
				</div>

				<div class="author1">
					<form:select path="author.authorId">
						<form:option value="0" label="0">
							<spring:message code="select.author" />
						</form:option>
						<form:options items="${authorList}" itemValue="authorId"
							itemLabel="authorName" />
					</form:select>
				</div>
				<div class="tags1">
					<form:select id="ms" multiple="true" path="tags">
						<form:options items="${tagList}" itemValue="tagId"
							itemLabel="tagName" />
					</form:select>
				</div>
				<script>
					$(function() {
						$('#ms').change(function() {
							console.log($(this).val());
						}).multipleSelect({
							width : '100%',
							selectAll : false,
							placeholder : "Select tag(s)"
						});
					});
				</script>
				<input class="add-news-button" type="submit"
					value="<spring:message code="button.save" />">
			</form:form>

		</div>
	</tiles:putAttribute>
</tiles:insertDefinition>
