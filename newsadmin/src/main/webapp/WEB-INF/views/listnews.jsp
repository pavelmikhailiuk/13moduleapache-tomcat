<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="ctg" uri="customtags" %>

<tiles:insertDefinition name="defaultTemplate">
	<tiles:putAttribute name="body">
		<div class="body">
			<div class="search">
				<form action="<c:url value="/admin/listnews"/>" method="get">
					<div class="author">
						<select name="authorId" size="1">
							<option value="0"><spring:message code="select.author" /></option>
							<c:forEach var="author" items="${authorList}">
								<option value="${author.authorId}">${author.authorName}</option>
							</c:forEach>
						</select>
					</div>
					<div class="tags">
						<select id="ms" multiple="multiple" name="tags" size="1">
							<c:forEach var="tag" items="${tagList}">
								<option value="${tag.tagId}">${tag.tagName}</option>
							</c:forEach>
						</select>
					</div>
					<script>
						$(function() {
							$('#ms').change(function() {
								console.log($(this).val());
							}).multipleSelect({
								width : '100%',
								selectAll : false,
								placeholder : "Select tag(s)"
							});
						});
					</script>
					<div class="filter">
						<input type="hidden" name="search" value="true" /> <input
							type="submit" value="<spring:message code="filter" />">
					</div>
				</form>
			</div>
			<div class="reset">
				<form action="<c:url value="/admin/listnews"/>" method="get">
					<input type="hidden" name="clear" value="true" /> <input
						type="submit" value="<spring:message code="reset" />">
				</form>
			</div>
			<form action="<c:url value="/admin/deletenews"/>" method="post">
			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
				<c:forEach var="newsDTO" items="${newsDTOList}">
					<c:set var="news" value="${newsDTO.news}" />
					<c:set var="author" value="${newsDTO.author}" />
					<c:set var="commentList" value="${newsDTO.comments}" />
					<c:set var="tags" value="${newsDTO.tags}" />
					<table class="list-news">
						<tr>

							<td><b><a
									href="<c:url value="/admin/shownews/${news.newsId}"/>"><c:out
											value="${news.title}" /></a></b> ( <spring:message code="author" />
								<c:out value="${author.authorName}" /> )</td>
							<td class="news-list-date"><fmt:formatDate
									value="${news.modificationDate}" /></td>
						</tr>
						<tr>

							<td class="news-list-brief" colspan="2"><c:out
									value="${news.shortText}" /></td>

						</tr>
						<tr>

							<td class="news-list-tags" colspan="2"><c:forEach var="tag"
									items="${tags}">
									<i><c:out value="${tag.tagName}" /></i>
								</c:forEach> <spring:message code="comments" /> ( <c:out
									value="${commentList.size()}" /> ) <a
								href="<c:url value="/admin/editnews/${news.newsId}"/>"><spring:message
										code="edit.news" /></a><input type="checkbox" name="deleteNewsId"
								value="${news.newsId}" /></td>
						</tr>
					</table>
				</c:forEach>

				<div class="delete">
					<input type="submit" value="<spring:message code="news.delete" />">
				</div>
			</form>
			
			<table class="pages">
				<tr>
					<ctg:pagination noOfPages="${noOfPages}"
						currentPage="${currentPage}" />
				</tr>
			</table>

		</div>
	</tiles:putAttribute>
</tiles:insertDefinition>
