<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>

<tiles:insertDefinition name="login">
	<tiles:putAttribute name="body">
		<div class="login-body">
			<div class="login">
				<c:if test="${not empty errCode}">
					<h1>${errCode}: System Error</h1>
				</c:if>

				<c:if test="${empty errCode}">
					<h1>System Error</h1>
				</c:if>

				<c:if test="${not empty errMsg}">
					<h5>${errMsg}</h5>
				</c:if>
			</div>

		</div>
	</tiles:putAttribute>
</tiles:insertDefinition>
