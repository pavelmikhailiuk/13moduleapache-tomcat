<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="header">
	<h1>
		<spring:message code="header.newsportal" />
	</h1>
	<c:url value="/j_spring_security_logout" var="logoutUrl" />
	<form action="${logoutUrl}" method="post" id="logoutForm">
		<input type="hidden" name="${_csrf.parameterName}"
			value="${_csrf.token}" />
		<c:if test="${pageContext.request.userPrincipal.name != null}">
			<h2>
				<spring:message code="header.hello" />
				${pageContext.request.userPrincipal.name} <input type="submit"
					value="<spring:message code="button.logout" />">
			</h2>
		</c:if>
	</form>
	<a href="?language=ru_RU">RU</a> <a href="?language=en_US">EN</a>
</div>