<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="ctg" uri="customtags" %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><tiles:getAsString name="title" /></title>
<link type="text/css" rel="stylesheet" href="<c:url value="/resources/css/style.css" />" />
<link type="text/css" rel="stylesheet" href="<c:url value="/resources/css/multiple-select.css" />"  />
<script src="<c:url value="/resources/js/jquery.min.js" />"></script>
<script src="<c:url value="/resources/js/jquery.multiple.select.js" />"></script>
<script src="<c:url value="/resources/errormessages.jsp" />"></script>
<script src="<c:url value="/resources/js/validation.js" />"></script>

</head>
<body>

	<tiles:insertAttribute name="header" />
	<tiles:insertAttribute name="menu" />
	<tiles:insertAttribute name="body" />
	<tiles:insertAttribute name="footer" />

</body>
</html>