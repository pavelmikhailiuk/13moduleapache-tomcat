package by.epam.newsadmin.controller;

import javax.servlet.http.HttpSession;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

/**
 * The Class MainController.
 */
@Controller
public class LoginController {

	/** The Constant VIEW_NAME_403. */
	private static final String VIEW_NAME_403 = "403";

	/** The Constant MODEL_ATTRIBUTE_USERNAME. */
	private static final String MODEL_ATTRIBUTE_USERNAME = "username";

	/** The Constant MESSAGE_LOGOUTMESSAGE. */
	private static final String MESSAGE_LOGOUTMESSAGE = "logoutmessage";

	/** The Constant MODEL_ATTRIBUTE_LOGOUT. */
	private static final String MODEL_ATTRIBUTE_LOGOUT = "logout";

	/** The Constant MESSAGE_ERRORMESSAGE. */
	private static final String MESSAGE_ERRORMESSAGE = "errormessage";

	/** The Constant MODEL_ATTRIBUTE_ERROR. */
	private static final String MODEL_ATTRIBUTE_ERROR = "error";

	/** The Constant VIEW_NAME_LOGIN. */
	private static final String VIEW_NAME_LOGIN = "login";

	/**
	 * Default page.
	 *
	 * @param model
	 *            the model and view object
	 * @return the model and view object
	 */
	@RequestMapping(value = { "/" }, method = RequestMethod.GET)
	public ModelAndView defaultPage(ModelAndView model, HttpSession session) {
		model.setViewName(VIEW_NAME_LOGIN);
		return model;
	}

	/**
	 * Login.
	 *
	 * @param error
	 *            the error
	 * @param logout
	 *            the logout
	 * @param model
	 *            the model and view object
	 * @return the model and view object
	 */
	@RequestMapping(value = { "/login" }, method = RequestMethod.GET)
	public ModelAndView login(@RequestParam(required = false) String error,
			@RequestParam(required = false) String logout, ModelAndView model) {
		if (error != null) {
			model.addObject(MODEL_ATTRIBUTE_ERROR, MESSAGE_ERRORMESSAGE);
		}
		if (logout != null) {
			model.addObject(MODEL_ATTRIBUTE_LOGOUT, MESSAGE_LOGOUTMESSAGE);
		}
		model.setViewName(VIEW_NAME_LOGIN);
		return model;
	}

	/**
	 * Access denied.
	 *
	 * @param model
	 *            the model and view object
	 * @return the model and view object
	 */
	@RequestMapping(value = { "/403" }, method = RequestMethod.GET)
	public ModelAndView accessDenied(ModelAndView model) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (!(auth instanceof AnonymousAuthenticationToken)) {
			UserDetails userDetail = (UserDetails) auth.getPrincipal();
			model.addObject(MODEL_ATTRIBUTE_USERNAME, userDetail.getUsername());
		}
		model.setViewName(VIEW_NAME_403);
		return model;
	}
}
