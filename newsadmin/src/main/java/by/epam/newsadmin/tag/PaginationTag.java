package by.epam.newsadmin.tag;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PaginationTag extends TagSupport {
	private final static Logger LOGGER = LoggerFactory
			.getLogger(PaginationTag.class);
	private int noOfPages;
	private int currentPage;

	public PaginationTag() {
	}

	public PaginationTag(int noOfPages, int currentPage) {
		this.noOfPages = noOfPages;
		this.currentPage = currentPage;
	}

	public void setNoOfPages(int noOfPages) {
		this.noOfPages = noOfPages;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	@Override
	public int doStartTag() throws JspException {
		try (JspWriter out = pageContext.getOut()) {
			out.write(createLink());
		} catch (IOException e) {
			LOGGER.error("Custom tag error", e);
		}
		return SKIP_BODY;
	}

	@Override
	public int doEndTag() throws JspException {
		return EVAL_PAGE;
	}

	private String createLink() {
		StringBuilder sb = new StringBuilder();
		HttpServletRequest request = (HttpServletRequest) pageContext
				.getRequest();
		for (int i = 1; i <= noOfPages; i++) {
			if (currentPage == i) {
				sb.append("<td>").append(i).append("</td>");
			} else {
				sb.append("<td><a href='").append(request.getContextPath())
						.append("/admin/listnews?pageNumber=").append(i)
						.append("'>").append(i).append("</a></td>");
			}
		}
		return sb.toString();
	}
}
