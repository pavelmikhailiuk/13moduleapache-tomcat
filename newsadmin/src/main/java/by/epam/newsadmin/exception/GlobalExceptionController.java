package by.epam.newsadmin.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.TypeMismatchException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

import by.epam.newscommon.exception.ServiceException;

/**
 * The Class GlobalExceptionController.
 */
@ControllerAdvice
public class GlobalExceptionController {
	
	private static final String TYPE_MISMATCH_EXCEPTION = "TypeMismatchException";
	private static final String EXCEPTION = "Exception";
	private static final String SERVICE_EXCEPTION = "ServiceException";
	private static final String ERROR_MESSAGE_INCORRECT_ID = "Incorrect Id";
	private static final String MODEL_ATTRIBUTE_ERR_MSG = "errMsg";
	private static final String ERROR_CODE_404 = "404";
	private static final String ERROR_CODE_500 = "500";
	private static final String MODEL_ATTRIBUTE_ERR_CODE = "errCode";
	private static final String VIEW_NAME_ERROR = "error";
	
	/** The Constant LOGGER. */
	public static final Logger LOGGER = LoggerFactory
			.getLogger(GlobalExceptionController.class);

	/**
	 * Handle service exception.
	 *
	 * @param ex the service exception
	 * @return the model and view object
	 */
	@ExceptionHandler(ServiceException.class)
	public ModelAndView handleServiceException(ServiceException ex) {
		LOGGER.error(SERVICE_EXCEPTION,ex);
		ModelAndView model = new ModelAndView(VIEW_NAME_ERROR);
		model.addObject(MODEL_ATTRIBUTE_ERR_CODE, ERROR_CODE_500);
		model.addObject(MODEL_ATTRIBUTE_ERR_MSG, ex.getMessage());
		return model;

	}

	/**
	 * Handle all exception.
	 *
	* @param ex the service exception
	 * @return the model and view object
	 */
	@ExceptionHandler(Exception.class)
	public ModelAndView handleAllException(Exception ex) {
		LOGGER.error(EXCEPTION,ex);
		ModelAndView model = new ModelAndView(VIEW_NAME_ERROR);
		model.addObject(MODEL_ATTRIBUTE_ERR_MSG, ex.getMessage());
		return model;

	}
	
	/**
	 * Handle type mismatch exception.
	 *
	* @param ex the type mismatch exception
	 * @return the model and view object
	 */
	@ExceptionHandler(TypeMismatchException.class)
	public ModelAndView handleTypeMismatchException(TypeMismatchException ex) {
		LOGGER.error(TYPE_MISMATCH_EXCEPTION,ex);
		ModelAndView model = new ModelAndView(VIEW_NAME_ERROR);
		model.addObject(MODEL_ATTRIBUTE_ERR_CODE, ERROR_CODE_404);
		model.addObject(MODEL_ATTRIBUTE_ERR_MSG, ERROR_MESSAGE_INCORRECT_ID);
		return model;

	}
}