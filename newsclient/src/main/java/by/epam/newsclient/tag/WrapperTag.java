package by.epam.newsclient.tag;

import java.io.IOException;

import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WrapperTag extends TagSupport {
	private static final Logger LOGGER = LoggerFactory.getLogger(WrapperTag.class);

	@Override
	public int doStartTag() {
		JspWriter out = pageContext.getOut();
		try {
			out.write("<table class='pages'><tr><td>");
		} catch (IOException e) {
			LOGGER.error("error writing start tag", e);
		}
		return EVAL_BODY_INCLUDE;
	}

	@Override
	public int doEndTag() {
		JspWriter out = pageContext.getOut();
		try {
			out.write("</td></tr></table>");
		} catch (IOException e) {
			LOGGER.error("error writing end tag", e);
		}
		return EVAL_PAGE;
	}
}
