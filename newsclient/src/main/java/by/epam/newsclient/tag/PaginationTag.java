package by.epam.newsclient.tag;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PaginationTag extends TagSupport {
	private final static Logger LOGGER = LoggerFactory.getLogger(PaginationTag.class);
	private int noOfPages;
	private int currentPage;

	public PaginationTag() {
	}

	public PaginationTag(int noOfPages, int currentPage) {
		this.noOfPages = noOfPages;
		this.currentPage = currentPage;
	}

	public int getNoOfPages() {
		return noOfPages;
	}

	public void setNoOfPages(int noOfPages) {
		this.noOfPages = noOfPages;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	@Override
	public int doStartTag() throws JspException {
		try (JspWriter out = pageContext.getOut()) {
			StringBuilder sb = new StringBuilder();
			for (int i = 1; i <= noOfPages; i++) {
				if (currentPage == i) {
					sb.append("<td>" + i + "</td>");
				} else {
					sb.append("<td><a href='Controller?page=list_news&pageNumber=" + i + "'>" + i + "</a></td>");
				}
			}
			out.write(sb.toString());
		} catch (IOException e) {
			LOGGER.error("Custom tag error", e);
		}

		return SKIP_BODY;
	}

	@Override
	public int doEndTag() throws JspException {
		return EVAL_PAGE;
	}
}