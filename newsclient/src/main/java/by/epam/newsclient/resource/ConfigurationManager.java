package by.epam.newsclient.resource;


import java.util.ResourceBundle;

/**
 * The Class ConfigurationManager.
 */
public class ConfigurationManager {
    
    /** The Constant RESOURCE_BUNDLE_NAME. */
    private static final String RESOURCE_BUNDLE_NAME = "config";
	
	/** The Constant resourceBundle. */
	private final static ResourceBundle resourceBundle = ResourceBundle.getBundle(RESOURCE_BUNDLE_NAME);

    /**
     * Instantiates a new configuration manager.
     */
    private ConfigurationManager() {
    }

    /**
     * Gets the property.
     *
     * @param key the key
     * @return the property
     */
    public static String getProperty(String key) {
        return resourceBundle.getString(key);
    }
}
