package by.epam.newsclient.navigation.command;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import by.epam.newsclient.navigation.Command;
import by.epam.newsclient.navigation.service.NewsPaginationProcessor;
import by.epam.newsclient.resource.ConfigurationManager;
import by.epam.newscommon.entity.NewsDTO;
import by.epam.newscommon.exception.ServiceException;
import by.epam.newscommon.service.NewsManagementService;

/**
 * The Class ShowNewsCommand.
 */
@Component(value = "show_news")
public class ShowNewsCommand implements Command {
	
	/** The Constant SESSION_ATTRIBUTE_PREV_NEWS_DTO. */
	private static final String REQUEST_ATTRIBUTE_PREV_NEWS_DTO = "prevNewsDTO";

	/** The Constant SESSION_ATTRIBUTE_NEXT_NEWS_DTO. */
	private static final String REQUEST_ATTRIBUTE_NEXT_NEWS_DTO = "nextNewsDTO";

	/** The Constant SESSION_ATTRIBUTE_NEWS_DTO. */
	private static final String REQUEST_ATTRIBUTE_NEWS_DTO = "newsDTO";

	/** The Constant SESSION_ATTRIBUTE_OFFSET. */
	private static final String SESSION_ATTRIBUTE_OFFSET = "offSet";

	/** The Constant VIEW_DIRECTION_EMPTY. */
	private static final String VIEW_DIRECTION_EMPTY = "empty";

	/** The Constant REQUEST_PARAMETER_VIEW_DIRECTION. */
	private static final String REQUEST_PARAMETER_VIEW_DIRECTION = "direction";

	/** The Constant REQUEST_PARAMETER_NEWS_ID. */
	private static final String REQUEST_PARAMETER_NEWS_ID = "newsId";

	/** The Constant NEWS_AT_PAGE. */
	public static final int NEWS_AT_PAGE = 3;

	/** The news management service. */
	@Autowired
	private NewsManagementService newsManagementService;

	/** The processor. */
	@Autowired
	private NewsPaginationProcessor processor;

	/*
	 * (non-Javadoc)
	 * 
	 * @see by.epam.newsclient.navigation.Command#execute(javax.servlet.http.
	 * HttpServletRequest)
	 */
	@Override
	public String execute(HttpServletRequest request) throws ServiceException {
		HttpSession session = request.getSession();
		long newsId = Long.parseLong(request.getParameter(REQUEST_PARAMETER_NEWS_ID));
		String direction = request.getParameter(REQUEST_PARAMETER_VIEW_DIRECTION);
		direction = direction == null ? VIEW_DIRECTION_EMPTY : direction;
		Integer offSet = processor.calculateOffSet(newsId, direction, session);
		NewsDTO newsDTO = newsManagementService.findNews(newsId);
		NewsDTO prevNewsDTO = findNewsDTO(request, offSet - 1);
		NewsDTO nextNewsDTO = findNewsDTO(request, offSet + 1);
		session.setAttribute(SESSION_ATTRIBUTE_OFFSET, offSet);
		request.setAttribute(REQUEST_ATTRIBUTE_NEWS_DTO, newsDTO);
		request.setAttribute(REQUEST_ATTRIBUTE_NEXT_NEWS_DTO, nextNewsDTO);
		request.setAttribute(REQUEST_ATTRIBUTE_PREV_NEWS_DTO, prevNewsDTO);
		return ConfigurationManager.getProperty("path.page.shownews");
	}

	/**
	 * Find news DTO.
	 *
	 * @param request
	 *            the request
	 * @param offSet
	 *            the offset
	 * @return the news DTO
	 * @throws ServiceException
	 *             the service exception
	 */
	private NewsDTO findNewsDTO(HttpServletRequest request, Integer offSet) throws ServiceException {
		List<NewsDTO> newsDTOList = processor.processNewsForPage(request, offSet, NEWS_AT_PAGE);
		return !newsDTOList.isEmpty() ? newsDTOList.get(0) : null;
	}
}
