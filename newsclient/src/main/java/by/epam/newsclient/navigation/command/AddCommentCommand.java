package by.epam.newsclient.navigation.command;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import by.epam.newsclient.navigation.Command;
import by.epam.newsclient.resource.ConfigurationManager;
import by.epam.newscommon.entity.Comment;
import by.epam.newscommon.entity.NewsDTO;
import by.epam.newscommon.exception.ServiceException;
import by.epam.newscommon.service.NewsManagementService;

/**
 * The Class AddCommentCommand.
 */
@Component(value = "add_comment")
public class AddCommentCommand implements Command {

	/** The Constant NOT_VALID_COMMENT_TEXT. */
	private static final String NOT_VALID_COMMENT_TEXT = "notValidCommentText";

	/** The Constant REQUEST_ATTRIBUTE_ERROR. */
	private static final String REQUEST_ATTRIBUTE_ERROR = "error";

	/** The Constant SESSION_ATTRIBUTE_OLD_COMMENT_HASH_CODE. */
	private static final String SESSION_ATTRIBUTE_OLD_COMMENT_HASH_CODE = "oldCommentHashCode";

	/** The Constant SESSION_ATTRIBUTE_NEWS_DTO. */
	private static final String REQUEST_ATTRIBUTE_NEWS_DTO = "newsDTO";

	/** The Constant REQUEST_PARAMETER_NEWS_ID. */
	private static final String REQUEST_PARAMETER_NEWS_ID = "newsId";

	/** The Constant REQUEST_PARAMETER_COMMENT_TEXT. */
	private static final String REQUEST_PARAMETER_COMMENT_TEXT = "commentText";

	/** The Constant SESSION_ATTRIBUTE_PREVIOUS_PAGE. */
	private static final String SESSION_ATTRIBUTE_PREVIOUS_PAGE = "previousPage";

	/** The Constant PREVIOUS_PAGE_SHOWNEWS. */
	private static final String PREVIOUS_PAGE_SHOWNEWS = "shownews";

	/** The news management service. */
	@Autowired
	private NewsManagementService newsManagementService;

	/*
	 * (non-Javadoc)
	 * 
	 * @see by.epam.newsclient.navigation.Command#execute(javax.servlet.http.
	 * HttpServletRequest)
	 */
	@Override
	public String execute(HttpServletRequest request) throws ServiceException {
		HttpSession session = request.getSession();
		String commentText = request.getParameter(REQUEST_PARAMETER_COMMENT_TEXT);
		long newsId = Long.parseLong(request.getParameter(REQUEST_PARAMETER_NEWS_ID));
		if (commentText != null && !commentText.equals("")) {
			Integer oldCommentHashCode = (Integer) session.getAttribute(SESSION_ATTRIBUTE_OLD_COMMENT_HASH_CODE);
			Integer commentHashCode = commentText.hashCode();
			if (oldCommentHashCode == null || !oldCommentHashCode.equals(commentHashCode)) {
				Comment comment = new Comment(1L, newsId, commentText, new Date());
				newsManagementService.saveComment(comment);
			}
			session.setAttribute(SESSION_ATTRIBUTE_OLD_COMMENT_HASH_CODE, commentHashCode);
		} else {
			request.setAttribute(REQUEST_ATTRIBUTE_ERROR, NOT_VALID_COMMENT_TEXT);
		}
		NewsDTO newsDTO = newsManagementService.findNews(newsId);
		request.setAttribute(REQUEST_ATTRIBUTE_NEWS_DTO, newsDTO);
		session.setAttribute(SESSION_ATTRIBUTE_PREVIOUS_PAGE, PREVIOUS_PAGE_SHOWNEWS);
		return ConfigurationManager.getProperty("path.page.shownews");
	}
}
