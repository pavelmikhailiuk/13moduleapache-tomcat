package by.epam.newsclient.navigation.command;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;

import by.epam.newsclient.navigation.Command;
import by.epam.newsclient.resource.ConfigurationManager;


/**
 * The Class ChangeLanguageCommand.
 */
@Component(value = "change_language")
public class ChangeLanguageCommand implements Command {
	
	/** The Constant PARAMETER_AND_ATTRIBUTE_LANGUAGE. */
	private static final String PARAMETER_AND_ATTRIBUTE_LANGUAGE = "language";
	
	/** The Constant SESSION_ATTRIBUTE_PREVIOUS_PAGE. */
	private static final String SESSION_ATTRIBUTE_PREVIOUS_PAGE = "previousPage";
	
	/** The Constant PREVIOUS_PAGE_LISTNEWS. */
	private static final String PREVIOUS_PAGE_LISTNEWS = "listnews";
	
	/** The Constant PREVIOUS_PAGE_SHOWNEWS. */
	private static final String PREVIOUS_PAGE_SHOWNEWS = "shownews";
	
	/* (non-Javadoc)
	 * @see by.epam.newsclient.navigation.Command#execute(javax.servlet.http.HttpServletRequest)
	 */
	@Override
	public String execute(HttpServletRequest request) {
		String previousPage = (String) request.getSession().getAttribute(
				SESSION_ATTRIBUTE_PREVIOUS_PAGE);
		switch (previousPage) {
		case PREVIOUS_PAGE_LISTNEWS:
			previousPage = "path.page.listnews";
			break;
		case PREVIOUS_PAGE_SHOWNEWS:
			previousPage = "path.page.shownews";
			break;
		}
		request.getSession().setAttribute(PARAMETER_AND_ATTRIBUTE_LANGUAGE,
				request.getParameter(PARAMETER_AND_ATTRIBUTE_LANGUAGE));
		return ConfigurationManager.getProperty(previousPage);
	}
}
