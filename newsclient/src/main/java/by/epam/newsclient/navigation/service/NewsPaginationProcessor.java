package by.epam.newsclient.navigation.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import by.epam.newscommon.entity.NewsDTO;
import by.epam.newscommon.entity.SearchCriteria;
import by.epam.newscommon.exception.ServiceException;
import by.epam.newscommon.service.NewsManagementService;


/**
 * The Class NewsPaginationProcessor.
 */
@Component
public class NewsPaginationProcessor {

	/** The Constant SESSION_ATTRIBUTE_NEWS_DTO_LIST. */
	private static final String SESSION_ATTRIBUTE_NEWS_DTO_LIST = "newsDTOList";
	
	/** The Constant VIEW_DIRECTION_NEXT. */
	private static final String VIEW_DIRECTION_NEXT = "next";
	
	/** The Constant VIEW_DIRECTION_PREV. */
	private static final String VIEW_DIRECTION_PREV = "prev";
	
	/** The Constant SESSION_ATTRUBUTE_OFF_SET. */
	private static final String SESSION_ATTRUBUTE_OFF_SET = "offSet";
	
	/** The Constant REQUEST_PARAMETER_TAGS. */
	private static final String REQUEST_PARAMETER_TAGS = "tags";
	
	/** The Constant REQUEST_PARAMETER_AUTHOR_ID. */
	private static final String REQUEST_PARAMETER_AUTHOR_ID = "authorId";
	
	/** The Constant REQUEST_PARAMETER_CLEAR. */
	private static final String REQUEST_PARAMETER_CLEAR = "clear";
	
	/** The Constant REQUEST_PARAMETER_SEARCH. */
	private static final String REQUEST_PARAMETER_SEARCH = "search";
	
	/** The Constant SESSION_ATTRIBUTE_SEARCH_CRITERIA. */
	private static final String SESSION_ATTRIBUTE_SEARCH_CRITERIA = "searchCriteria";
	
	/** The news count. */
	private long newsCount;

	/**
	 * Gets the news count.
	 *
	 * @return the news count
	 */
	public long getNewsCount() {
		return newsCount;
	}

	/** The news management service. */
	@Autowired
	private NewsManagementService newsManagementService;
	
	/** The initializer. */
	@Autowired
	private EntityInitializer initializer;

	/**
	 * Process news for page.
	 *
	 * @param request the request
	 * @param offset the offset
	 * @param newsAtPage the quantity of the news at page
	 * @return the list of news DTO object
	 * @throws ServiceException the service exception thrown if service operation failed
	 */
	public List<NewsDTO> processNewsForPage(HttpServletRequest request,
			int offset, int newsAtPage) throws ServiceException {
		HttpSession session = request.getSession();
		List<NewsDTO> newsDTOList = null;
		SearchCriteria searchCriteria = (SearchCriteria) session
				.getAttribute(SESSION_ATTRIBUTE_SEARCH_CRITERIA);
		String search = request.getParameter(REQUEST_PARAMETER_SEARCH);
		String clear = request.getParameter(REQUEST_PARAMETER_CLEAR);
		String authId = request.getParameter(REQUEST_PARAMETER_AUTHOR_ID);
		Long authorId = authId != null ? Long.parseLong(authId) : null;
		String[] tags = request.getParameterValues(REQUEST_PARAMETER_TAGS);
		searchCriteria = clear != null ? null : searchCriteria;
		searchCriteria = search != null ? initializer.initSearchCriteria(
				authorId, tags) : searchCriteria;
		if (searchCriteria != null) {
			newsCount = newsManagementService.countNews(searchCriteria);
			newsDTOList = newsManagementService.findNewsBySearchCriteria(
					searchCriteria, offset, newsAtPage);
		} else {
			newsCount = newsManagementService.countNews();
			newsDTOList = newsManagementService.findAllNews(offset, newsAtPage);
		}
		session.setAttribute(SESSION_ATTRIBUTE_SEARCH_CRITERIA, searchCriteria);
		return newsDTOList;
	}

	/**
	 * Calculate off set.
	 *
	 * @param newsId the news id
	 * @param direction the news view direction
	 * @param session the session
	 * @return the integer
	 */
	public Integer calculateOffSet(Long newsId, String direction,
			HttpSession session) {
		Integer offSet = (Integer) session.getAttribute(SESSION_ATTRUBUTE_OFF_SET);
		if (direction.equals(VIEW_DIRECTION_PREV) | direction.equals(VIEW_DIRECTION_NEXT)) {
			offSet = direction.equals(VIEW_DIRECTION_PREV) ? --offSet : offSet;
			offSet = direction.equals(VIEW_DIRECTION_NEXT) ? ++offSet : offSet;
		} else {
			@SuppressWarnings("unchecked")
			List<NewsDTO> newsDTOList = (List<NewsDTO>) session
					.getAttribute(SESSION_ATTRIBUTE_NEWS_DTO_LIST);
			if (newsDTOList != null) {
				for (int i = 0; i < newsDTOList.size(); i++) {
					NewsDTO tempNewsDTO = newsDTOList.get(i);
					if (tempNewsDTO.getNews().getNewsId() == newsId) {
						offSet += i;
					}
				}
			}
		}
		return offSet;
	}
}
