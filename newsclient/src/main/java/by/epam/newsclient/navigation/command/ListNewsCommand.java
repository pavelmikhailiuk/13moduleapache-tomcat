package by.epam.newsclient.navigation.command;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import by.epam.newsclient.navigation.Command;
import by.epam.newsclient.navigation.service.NewsPaginationProcessor;
import by.epam.newsclient.resource.ConfigurationManager;
import by.epam.newscommon.entity.*;
import by.epam.newscommon.exception.ServiceException;
import by.epam.newscommon.service.AuthorService;
import by.epam.newscommon.service.TagService;

/**
 * The Class ListNewsCommand.
 */
@Component(value = "list_news")
public class ListNewsCommand implements Command {
	
	/** The Constant SESSION_ATTRIBUTE_OLD_COMMENT_HASH_CODE. */
	private static final String SESSION_ATTRIBUTE_OLD_COMMENT_HASH_CODE = "oldCommentHashCode";
	/** The Constant PREVIOUS_PAGE_LISTNEWS. */
	private static final String PREVIOUS_PAGE_LISTNEWS = "listnews";

	/** The Constant SESSION_ATTRIBUTE_PREVIOUS_PAGE. */
	private static final String SESSION_ATTRIBUTE_PREVIOUS_PAGE = "previousPage";

	/** The Constant SESSION_ATTRIBUTE_TAG_LIST. */
	private static final String REQUEST_ATTRIBUTE_TAG_LIST = "tagList";

	/** The Constant SESSION_ATTRIBUTE_AUTHOR_LIST. */
	private static final String REQUEST_ATTRIBUTE_AUTHOR_LIST = "authorList";

	/** The Constant SESSION_ATTRIBUTE_CURRENT_PAGE. */
	private static final String SESSION_ATTRIBUTE_CURRENT_PAGE = "currentPage";

	/** The Constant SESSION_ATTRIBUTE_NO_OF_PAGES. */
	private static final String REQUEST_ATTRIBUTE_NO_OF_PAGES = "noOfPages";

	/** The Constant REQUEST_PARAMETER_PAGE_NUMBER. */
	private static final String REQUEST_PARAMETER_PAGE_NUMBER = "pageNumber";

	/** The Constant SESSION_ATTRIBUTE_OFFSET. */
	private static final String SESSION_ATTRIBUTE_OFFSET = "offSet";

	/** The Constant SESSION_ATTRIBUTE_NEWS_DTO_LIST. */
	private static final String REQUEST_ATTRIBUTE_NEWS_DTO_LIST = "newsDTOList";

	/** The Constant NEWS_AT_PAGE. */
	public static final int NEWS_AT_PAGE = 3;

	/** The author service. */
	@Autowired
	private AuthorService authorService;

	/** The tag service. */
	@Autowired
	private TagService tagService;

	/** The processor. */
	@Autowired
	private NewsPaginationProcessor processor;

	/*
	 * (non-Javadoc)
	 * 
	 * @see by.epam.newsclient.navigation.Command#execute(javax.servlet.http.
	 * HttpServletRequest)
	 */
	@Override
	public String execute(HttpServletRequest request) throws ServiceException {
		HttpSession session = request.getSession();
		session.removeAttribute(SESSION_ATTRIBUTE_OLD_COMMENT_HASH_CODE);
		List<Author> authorList = authorService.findAll();
		List<Tag> tagList = tagService.findAll();
		String pageNum = request.getParameter(REQUEST_PARAMETER_PAGE_NUMBER);
		int pageNumber = pageNum != null ? Integer.parseInt(pageNum) : 1;
		int offSet = (pageNumber - 1) * NEWS_AT_PAGE;
		List<NewsDTO> newsDTOList = processor.processNewsForPage(request, offSet, NEWS_AT_PAGE);
		long newsCount = processor.getNewsCount();
		long noOfPages = newsCount % NEWS_AT_PAGE == 0 ? newsCount / NEWS_AT_PAGE : (newsCount / NEWS_AT_PAGE) + 1;
		request.setAttribute(REQUEST_ATTRIBUTE_NEWS_DTO_LIST, newsDTOList);
		request.setAttribute(REQUEST_ATTRIBUTE_NO_OF_PAGES, noOfPages);
		session.setAttribute(SESSION_ATTRIBUTE_OFFSET, offSet);
		session.setAttribute(SESSION_ATTRIBUTE_CURRENT_PAGE, pageNumber);
		request.setAttribute(REQUEST_ATTRIBUTE_AUTHOR_LIST, authorList);
		request.setAttribute(REQUEST_ATTRIBUTE_TAG_LIST, tagList);
		session.setAttribute(SESSION_ATTRIBUTE_PREVIOUS_PAGE, PREVIOUS_PAGE_LISTNEWS);
		return ConfigurationManager.getProperty("path.page.listnews");
	}

}
