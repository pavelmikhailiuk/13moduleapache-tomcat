package by.epam.newsclient.navigation.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import by.epam.newscommon.entity.*;

/**
 * The Class EntityInitializer.
 */
@Component
public class EntityInitializer {

	/**
	 * Inits the search criteria object.
	 *
	 * @param authorId the author id
	 * @param tags the tags id array
	 * @return the search criteria object
	 */
	public SearchCriteria initSearchCriteria(Long authorId, String[] tags) {
		return new SearchCriteria(initAuthor(authorId), initTags(tags));
	}

	/**
	 * Inits the author.
	 *
	 * @param authorId the author id
	 * @return the author entity
	 */
	private Author initAuthor(Long authorId) {
		return authorId != 0 ? new Author(authorId, null, null) : null;
	}

	/**
	 * Inits the tags.
	 *
	 * @param tags the tags
	 * @return the list of tag object
	 */
	private List<Tag> initTags(String[] tags) {
		List<Tag> tagList = null;
		if (tags != null) {
			tagList = new ArrayList<>();
			for (int i = 0; i < tags.length; i++) {
				Long tagId = tags[i] != null ? Long.parseLong(tags[i]) : null;
				tagList.add(new Tag(tagId, null));
			}
		}
		return tagList;
	}
}
