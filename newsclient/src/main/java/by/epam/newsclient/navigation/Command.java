package by.epam.newsclient.navigation;

import javax.servlet.http.HttpServletRequest;

import by.epam.newscommon.exception.ServiceException;


/**
 * The Interface Command.
 */
public interface Command {
    
    /**
     * Execute.
     *
     * @param request the request
     * @return the string
     * @throws ServiceException the service exception thrown if service operation failed
     */
    String execute(HttpServletRequest request) throws ServiceException;
}
