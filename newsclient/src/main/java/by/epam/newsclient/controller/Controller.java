package by.epam.newsclient.controller;

import java.io.IOException;

import javax.servlet.*;
import javax.servlet.http.*;

import org.apache.log4j.Logger;
import org.springframework.web.context.support.XmlWebApplicationContext;

import by.epam.newsclient.navigation.Command;
import by.epam.newscommon.exception.ServiceException;

/**
 * The Class Controller.
 */
public class Controller extends HttpServlet {

	/** The Constant REQUEST_PARAMETER_PAGE. */
	public static final String REQUEST_PARAMETER_PAGE = "page";

	/** The Constant serialVersionUID. */
	public static final long serialVersionUID = 1L;

	/** The Constant LOGGER. */
	private final static Logger LOGGER = Logger.getLogger(Controller.class);

	private XmlWebApplicationContext context;

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.GenericServlet#init(javax.servlet.ServletConfig)
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		context = new XmlWebApplicationContext();
		context.setServletContext(config.getServletContext());
		context.refresh();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.
	 * HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		performAction(request, response);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.
	 * HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		performAction(request, response);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.GenericServlet#destroy()
	 */
	@Override
	public void destroy() {
		super.destroy();
	}

	/**
	 * Perform action.
	 *
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws ServletException
	 *             the servlet exception thrown if problems during initializing
	 *             or request processing.
	 */
	private void performAction(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		Command command = (Command) context.getBean(request.getParameter(REQUEST_PARAMETER_PAGE));
		String nextPage = null;
		try {
			nextPage = command.execute(request);
		} catch (ServiceException e) {
			response.sendError(500);
			LOGGER.error(e);
		}
		if (nextPage != null) {
			RequestDispatcher requestDispatcher = request.getRequestDispatcher(nextPage);
			requestDispatcher.forward(request, response);
		}
	}
}
