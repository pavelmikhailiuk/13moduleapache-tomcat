<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<fmt:setLocale value="${language}" scope="session" />
<fmt:setBundle basename="webResources" />
<div class="header">
	<h1>
		<fmt:message key="header.newsportal" />
	</h1>
	<c:set var="previousPage" value="${previousPage}" />
	<a
		href="Controller?language=ru_RU&page=change_language&prevPage=previousPage">RU</a>
	<a
		href="Controller?language=en_US&page=change_language&prevPage=previousPage">EN</a>
</div>