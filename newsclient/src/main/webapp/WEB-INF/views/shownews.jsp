<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<fmt:setLocale value="${language}" scope="session" />
<fmt:setBundle basename="webResources" />

<tiles:insertDefinition name="defaultTemplate">
	<tiles:putAttribute name="body">
		<div class="body">
			<div class=back-link>
				<a href="Controller?page=list_news&pageNumber=${currentPage}"><fmt:message
						key="back" /></a>
			</div>
			<c:set var="news" value="${newsDTO.news}" />
			<c:set var="prevNews" value="${prevNewsDTO.news}" />
			<c:set var="nextNews" value="${nextNewsDTO.news}" />
			<c:set var="author" value="${newsDTO.author}" />
			<c:set var="commentList" value="${newsDTO.comments}" />
			<table class="list-news">
				<tr>

					<td><b><c:out value="${news.title}" /></b> ( <fmt:message
							key="author" /> <c:out value="${author.authorName}" /> )</td>
					<td class="news-list-date"><fmt:formatDate
							value="${news.modificationDate}" /></td>
				</tr>
				<tr>

					<td class="news-list-brief" colspan="2"><c:out
							value="${news.fullText}" /></td>
				</tr>
			</table>
			<c:forEach var="comment" items="${commentList}">
				<fmt:formatDate value="${comment.creationDate}" />
				<div class="comment-text">
					<c:out value="${comment.commentText}" />
				</div>
			</c:forEach>
			<form name="commentForm" action="Controller" method="post"
				onsubmit="return validateCommentForm();">
				<textarea class="textarea" cols="40" rows="5" name="commentText"
					maxlength="100"></textarea>
				<c:if test="${error !=null}">
					<span class="error"> <fmt:message key="error.comment" /></span>
				</c:if>
				<br /> <input type="hidden" name="newsId" value="${news.newsId}" />
				<input type="hidden" name="page" value="add_comment" />
				<button class="post-comment-button" type="submit">
					<fmt:message key="post.comment" />
				</button>
			</form>
			<c:if test="${prevNews != null}">
				<div class="prev">
					<a
						href="Controller?page=show_news&newsId=${prevNews.newsId}&direction=prev"><fmt:message
							key="previous" /></a>
				</div>
			</c:if>
			<c:if test="${nextNews != null}">
				<div class="next">
					<a
						href="Controller?page=show_news&newsId=${nextNews.newsId}&direction=next"><fmt:message
							key="next" /></a>
				</div>
			</c:if>
		</div>
	</tiles:putAttribute>
</tiles:insertDefinition>
