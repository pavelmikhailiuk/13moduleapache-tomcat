<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="ctg" uri="customtags"%>
<fmt:setLocale value="${language}" scope="session" />
<fmt:setBundle basename="webResources" />


<tiles:insertDefinition name="defaultTemplate">
	<tiles:putAttribute name="body">
		<div class="body">
			<div class="search">
				<form action="Controller" method="post">
					<div class="author">
						<select name="authorId" size="1">
							<option value="0"><fmt:message key="select.author" /></option>
							<c:forEach var="author" items="${authorList}">
								<option value="${author.authorId}">${author.authorName}</option>
							</c:forEach>
						</select>
					</div>
					<div class="tags">
						<select id="ms" multiple="multiple" name="tags" size="1">
							<c:forEach var="tag" items="${tagList}">
								<option value="${tag.tagId}">${tag.tagName}</option>
							</c:forEach>
						</select>
					</div>
					<script>
						$(function() {
							$('#ms').change(function() {
								console.log($(this).val());
							}).multipleSelect({
								width : '100%',
								selectAll : false,
								placeholder : "Select tag(s)"
							});
						});
					</script>
					<div class="filter">
						<input type="hidden" name="page" value="list_news" /> <input
							type="hidden" name="search" value="true" /> <input type="submit"
							value="<fmt:message key="filter"/>">
					</div>
				</form>
			</div>
			<div class="reset">
				<form action="Controller" method="post">
					<input type="hidden" name="page" value="list_news" /><input
						type="hidden" name="clear" value="true" /> <input type="submit"
						value="<fmt:message key="reset"/>">
				</form>
			</div>

			<c:forEach var="newsDTO" items="${newsDTOList}">
				<c:set var="news" value="${newsDTO.news}" />
				<c:set var="author" value="${newsDTO.author}" />
				<c:set var="commentList" value="${newsDTO.comments}" />
				<c:set var="tags" value="${newsDTO.tags}" />
				<table class="list-news">
					<tr>

						<td><b><c:out value="${news.title}" /></b> ( <fmt:message
								key="author" /> <c:out value="${author.authorName}" /> )</td>
						<td class="news-list-date"><fmt:formatDate
								value="${news.modificationDate}" /></td>
					</tr>
					<tr>

						<td class="news-list-brief" colspan="2"><c:out
								value="${news.shortText}" /></td>

					</tr>
					<tr>

						<td class="news-list-tags" colspan="2"><c:forEach var="tag"
								items="${tags}">
								<c:out value="${tag.tagName}" />
							</c:forEach> <fmt:message key="comments" /> ( <c:out
								value="${commentList.size()}" /> ) <a
							href="Controller?newsId=${news.newsId}&page=show_news"><fmt:message
									key="view.news" /></a></td>
					</tr>
				</table>
			</c:forEach>

			<table class="pages">
				<tr>
					<ctg:pagination noOfPages="${noOfPages}"
						currentPage="${currentPage}" />
				</tr>
			</table>

		</div>
	</tiles:putAttribute>
</tiles:insertDefinition>
