<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<tiles:insertDefinition name="errorTemplate">
	<tiles:putAttribute name="body">
		<div class="login-body">
			<div class="login">

				<h1>500 Internal server error</h1>
				<h1>Sorry</h1>

			</div>

		</div>
	</tiles:putAttribute>
</tiles:insertDefinition>
